package CodeForce;

import java.util.Scanner;

public class DoubleCola {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.println(method(n));
    }

    public static String method(int n) {
        int s = 1;
        int l = 2;
        int p = 3;
        int r = 4;
        int h = 5;

        int m = 1;
        int q = 5;
        for (int i = 1; i <= n; i++) {
            if (n > s + m - 1) {
                s += q + m;
            } else {
                return "Sheldon";
            }
            if (n > l + m - 1) {
                l += q + m;
            } else {
                return "Leonard";
            }
            if (n > p + m - 1) {
                p += q + m;
            } else {
                return "Penny";
            }
            if (n > r + m - 1) {
                r += q + m;
            } else {
                return "Rajesh";
            }
            if (n > h + m - 1) {
                h += q + m;
            } else {
                return "Howard";
            }
            q *= 2;
            m ^= 2;
        }
        return null;
    }
}