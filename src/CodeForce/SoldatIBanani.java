package CodeForce;

import java.util.Scanner;

public class SoldatIBanani {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int k = in.nextInt();
        int n = in.nextInt();
        int w = in.nextInt();
        int n2 = 0;
        for (int i = w; i > 0; i--) {
            n2 += i * k;
        }
        System.out.println(n < n2? n2 - n: 0);
    }
}
