package CodeForce;

import java.util.Scanner;

public class KorotkiePodstroki {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            String b = in.next();
            System.out.println(method(b));

        }
    }
    public static String method(String b) {
        char[] a = new char[b.length() / 2 + 1];
        int count = 1;
        a[0] = b.charAt(0);
        for (int i = 1; i <= b.length() - 1; i += 2) {
                a[count] = b.charAt(i);
                count++;
                if (a.length == b.toCharArray().length / 2) {
                    a[a.length - 1] = b.charAt(b.length() - 1);
                    break;
            }
        }
        String a2 = new String(a);
        return a2;
    }
}
