package Homeworks.OOP;

class Burger {
    private static int count;
    private String bun;
    private String sesame;
    private String meat;
    private String lettuce;
    private String tomato;
    private String cucumber;
    private String sauce;

    Burger(String bun, String sesame, String meat, String lettuce, String tomato, String cucumber, String sauce) {
        this.bun = bun;
        this.sesame = sesame;
        this.meat = meat;
        this.lettuce = lettuce;
        this.tomato = tomato;
        this.cucumber = cucumber;
        this.sauce = sauce;
        count++;
    }

    public static int getCount() {
        return count;
    }

    public String getBun() {
        return bun;
    }

    public void setBun(String bun) {
        if (bun.equals("темная булочка") || bun.equals("булочка")) {
            this.bun = bun;
        } else {
            System.out.println("такой булочки нет!");
        }
    }

    public String getSesame() {
        return sesame;
    }

    public void setSesame(String sesame) {
        if (sesame.equals("белый кунжут") || sesame.equals("темный кунжут") || sesame.equals("нет кунжута")) {
            this.sesame = sesame;
        } else {
            System.out.println("такого кунжута нет!");
        }
    }

    public String getMeat() {
        return meat;
    }

    public void setMeat(String meat) {
        if (meat.equals("говяжая котлета") || meat.equals("куриная котлета") || meat.equals("рыбная котлета")) {
            this.meat = meat;
        } else {
            System.out.println("такого мяса нет!");
        }
    }

    public String getLettuce() {
        return lettuce;
    }

    public void setLettuce(String lettuce) {
        this.lettuce = lettuce;
    }

    public String getTomato() {
        return tomato;
    }

    public void setTomato(String tomato) {
        this.tomato = tomato;
    }

    public String getCucumber() {
        return tomato;
    }

    public void setCucumber(String cucumber) {
        this.cucumber = cucumber;
    }

    public String getSauce() {
        return sauce;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }
}
