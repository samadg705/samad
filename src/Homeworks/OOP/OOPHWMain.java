package Homeworks.OOP;

public class OOPHWMain {
    public static void main(String[] args) {
        System.out.println(Homeworks.OOP.Burger.getCount());
        Burger Burger = new Burger("обычная булочка", "белый кунжут", "говяжая котлета", "лист салата", "слайсы помидора", "слайсы соленого огурца ","горцунез");
        Burger DarkBurger = new Burger("темная булочка", "темный кунжут", "говяжая котлета", "лист салата", "слайсы помидора", "слайсы соленого огурца ", "горцунез + кетчунез");
        Burger ChickenBurger = new Burger("обычная булочка", "белый кунжут", "куриная котлета", "лист салата", "слайсы помидора", "слайсы соленого огурца ", "горцунез");
        Burger FishBurger = new Burger("обычная булочка","нет кунжута", "рыбная котлета", "лист салата", "слайсы помидора", "слайсы соленого огурца ", "сметана");
        System.out.println(Homeworks.OOP.Burger.getCount());

    }
}
