package Homeworks.OOP;

public class SimpleBurger {
    public static Burger simpleBurger = new Burger("обычная булочка", "белый кунжут", "говяжая котлета",
            "лист салата", "слайсы помидора", "слайсы соленого огурца ", "горцунез");

    public static void main(String[] args) {
        String BurgerBun = simpleBurger.getBun();
        String BurgerSesame = simpleBurger.getSesame();
        String BurgerMeat = simpleBurger.getMeat();
        String BurgerLettuce = simpleBurger.getLettuce();
        String BurgerTomato = simpleBurger.getTomato();
        String BurgerCucumber = simpleBurger.getCucumber();
        String BurgerSauce = simpleBurger.getSauce();
        System.out.println(BurgerBun + ", " + BurgerSesame + ", " + BurgerMeat + ", " + BurgerLettuce + ", " + BurgerTomato + ", " + BurgerCucumber + ", " + BurgerSauce);
    }
}