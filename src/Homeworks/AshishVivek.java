package Homeworks;

import java.util.Scanner;

public class AshishVivek {
    private static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        int count = in.nextInt();
        for (int i = 0; i < count; i++) {
            game();
        }
    }

    private static void game() {
        int n = in.nextInt();
        int m = in.nextInt();
        int countN = n;
        int countM = m;
        int[][] field = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                field[i][j] = in.nextInt();
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print(field[i][j] + " ");
            }
            System.out.println("");
        }
        for (int i = 0; i < n; i++){
            for (int j = 0; j < m; j++){
                if (field[i][j] == 1){
                    countN--;
                    break;
                }
            }
        }
        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                if (field[j][i] == 1){
                    countM--;
                    break;
                }
            }
        }
        System.out.println(countN + " " + countM);
    }
}