package Homeworks.String_HW;

import java.util.Scanner;

class StringHWTask2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String word = in.nextLine();
        char[] word2 = word.toCharArray();
        System.out.println(word(word2));
    }

    public static boolean word(char[] word2) {
        for (int i = 0; i < word2.length; i++) {
            for (int j = i + 1; j < word2.length; j++) {
               if (word2[i] == word2[j]) {
                   return false;
               }
            }
        }
        return true;
    }
}
