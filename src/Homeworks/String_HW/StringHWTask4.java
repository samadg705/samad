package Homeworks.String_HW;

class StringHWTask4 {
    public static void main(String[] args) {
        String word3 = "абв";
        String word4 = "бвг";
        char[] word1 = word3.toCharArray();
        char[] word2 = word4.toCharArray();
        System.out.println(word(word1, word2));
    }

    public static boolean word(char[] word1, char[] word2) {
        int n;
        int m;
        int p;
        int c;
        int x = 0;
        p = Math.min(word1.length, word2.length);
        for (int i = 0; i < p; i++) {
            for (int j = 0; j < p; j++) {
                if (word1[i] == word2[j]) {
                    c = Math.max(i, j);
                    for (int q = 0; q < p - c; q++) {
                        n = i + q;
                        m = j + q;
                        if (word1[n] != word2[m]) {
                            x = 0;
                            break;
                        } else {
                            x = x + 1;
                            if (x == p - c) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
}