package Homeworks.String_HW;

import java.util.Scanner;

class StringHWTask1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String a = in.nextLine();
        System.out.println(a(a));
    }

    public static boolean a(String word) {
        char[] word2 = word.toCharArray();
        for (int i = 0; i < word2.length / 2; i++) {
            if (word2[i] != word2[word2.length - 1 - i]) {
                return false;
            }
        }
        return true;
    }
}