package Homeworks.String_HW;

import java.util.Scanner;

class StringHWTask5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String words = in.nextLine();
        System.out.println(countWord(words));
    }

    public static int countWord(String words) {
        int count = 0;
        for (int i = 0; i < words.toCharArray().length; i++) {
            if (i == words.toCharArray().length - 1) {
                break;
            }
            if (((int) words.charAt(i) == 32) && ((((int) words.charAt(i + 1) > 127) && ((int) words.charAt(i + 1) < 160))
                    || (((int) words.charAt(i + 1) > 64) && ((int) words.charAt(i + 1) < 91)))) {
                count++;
            }
            if (i == 0) {
                if ((((int) words.charAt(i) > 127) && ((int) words.charAt(i) < 160))
                        || (((int) words.charAt(i) > 64) && ((int) words.charAt(i) < 91))) {
                    count++;
                }
            }
        }
        return count;
    }
}