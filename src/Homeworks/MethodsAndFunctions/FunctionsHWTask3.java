package Homeworks.MethodsAndFunctions;

import java.util.Scanner;

class FunctionsHWTask3 {
    public static boolean xORoMethod(int i) {
        boolean xORo;
        if (i % 2 == 0) {
            return xORo = true;
        } else {
            return xORo = false;
        }
    }
    public static String WinMethod(int i) {
        int j = i % 2;
        return switch (j) {
            case 1 -> "игрок №1 выиграл";
            case 0 -> "игрок №2 выиграл";
            default -> "";
        };
    }

    public static void main(String[] args) {
        int x2;
        int y2;
        int y;
        int x;
        Scanner in = new Scanner(System.in);
        int[][] field = new int[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                field[i][j] = 0;
            }
        }
        boolean xORo = true;
        for (int i = 1; i < 10; i++) {
            if (i == 1) {
                for (int n = 0; n < 3; n++) {
                    for (int m = 0; m < 3; m++) {
                        System.out.print(field[n][m] + " ");
                    }
                    System.out.println();
                }
                System.out.println("Начать игру?(да = 1, нет = 0)");
            } else {
                for (int n = 0; n < 3; n++) {
                    for (int m = 0; m < 3; m++) {
                        System.out.print(field[n][m] + " ");
                    }
                    System.out.println();
                }
                System.out.println("Продолжить игру?(да = 1, нет = 0)");
            }
            int YesOrNo = in.nextInt();
            if (YesOrNo == 1) {
            } else {
                System.out.println("ладно:(");
                break;
            }
            System.out.println("Ходите");
            System.out.println(!xORoMethod(i) ? "куда поставить 1 по осе Х?" : "куда поставить 2 по осе Х?");
            x2 = in.nextInt();
            x = switch (x2) {
                case 1 -> 0;
                case 2 -> 1;
                default -> 2;
            };
            System.out.println(!xORoMethod(i) ? "куда поставить 1 по осе Y?" : "куда поставить 2 по осе Y?");
            y2 = in.nextInt();
            y = switch (y2) {
                case 1 -> 2;
                case 2 -> 1;
                default -> 0;
            };
            if (!xORoMethod(i)) {
                field[y][x] = 1;
            } else {
                field[y][x] = 2;
            }
            if (((field[0][0] == 1) && (field[1][0] == 1) && (field[2][0] == 1)) || ((field[0][0] == 2) && (field[1][0] == 2) && (field[2][0] == 2))
                    || ((field[0][1] == 1) && (field[1][1] == 1) && (field[2][1] == 1)) || ((field[0][1] == 2) && (field[1][1] == 2) && (field[2][1] == 2))
                    || ((field[0][2] == 1) && (field[1][2] == 1) && (field[2][2] == 1)) || ((field[0][2] == 2) && (field[1][2] == 2) && (field[2][2] == 2))
                    || ((field[0][0] == 1) && (field[0][1] == 1) && (field[0][2] == 1)) || ((field[0][0] == 2) && (field[0][1] == 2) && (field[0][2] == 2))
                    || ((field[1][0] == 1) && (field[1][1] == 1) && (field[1][2] == 1)) || ((field[1][0] == 2) && (field[1][1] == 2) && (field[1][2] == 2))
                    || ((field[2][0] == 1) && (field[2][1] == 1) && (field[2][2] == 1)) || ((field[2][0] == 2) && (field[2][1] == 2) && (field[2][2] == 2))
                    || ((field[0][0] == 1) && (field[1][1] == 1) && (field[2][2] == 1)) || ((field[0][0] == 2) && (field[1][1] == 2) && (field[2][2] == 2))
                    || ((field[2][0] == 1) && (field[1][1] == 1) && (field[0][2] == 1)) || ((field[2][0] == 2) && (field[1][1] == 2) && (field[0][2] == 2))) {
                System.out.println(WinMethod(i));
                break;
            }
        }
    }
}