package Homeworks.MethodsAndFunctions;

import java.util.Scanner;

class FunctionsHWTask2 {
    private static int function(int a, int count) {
        for (int i = a; i > 9; i = i / 10) {
            count++;
        }
        return count;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите число");
        int a = in.nextInt();
        int count = 1;
        int x = function(a, count);
        String y = switch (x) {
            case 1 -> " разряд";
            case 2, 3, 4 -> " разряда";
            default -> " разрядов";
        };
        System.out.println("В числе " + a + " " + x + y);
    }
}