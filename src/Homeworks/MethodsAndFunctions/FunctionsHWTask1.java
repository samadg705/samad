package Homeworks.MethodsAndFunctions;

import java.util.Scanner;

class FunctionsHWTask1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите границы отрезка");
        System.out.println("1-ый конец:");
        int x = in.nextInt();
        System.out.println("2-ой конец:");
        int y = in.nextInt();
        System.out.println("Введите число");
        int z = in.nextInt();
        int a = 1;
        System.out.println(method(x, y, z, a) == 2 ? z + " принадлежит отрезку" : z + " не принадлежит отрезку");
    }

    public static int method(int x, int y, int z, int a) {
        if (x < y) {
            for (int i = x + 1; i < y; i++) {
                if (i == z) {
                    a++;
                    break;
                }
            }
        } else {
            for (int i = y + 1; i < x; i++) {
                if (i == z) {
                    a++;
                    break;
                }
            }
        }
        return a;
    }
}