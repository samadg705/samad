package Homeworks.Comparable;

import java.io.Serializable;
import java.util.Comparator;

public class People implements Comparable<People>, Serializable {
    private int age;
    private String name;
    private int height;
    private int weight;

    public People(int age, String name, int height, int weight) {
        this.age = age;
        this.name = name;
        this.height = height;
        this.weight = weight;
    }

    public People() {

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String toString() {
        return "Возраст: " + this.age + ", имя: " + this.name + ", рост: " + this.height + ", вес: " + this.weight;
    }

    @Override
    public int compareTo(People o) {
        int result = age + name.length() + height + weight;
        int result2 = o.age + o.name.length() + o.height + o.weight;
        return result == result2 ? 0 : result > result2 ? 1 : -1;
    }

    public static Comparator<People> AgeComparator = new Comparator<>() {
        @Override
        public int compare(People o1, People o2) {
            return o1.getAge() - o2.getAge();
        }
    };
    public static Comparator<People> NameComparator = new Comparator<>() {
        @Override
        public int compare(People o1, People o2) {
            return o1.getName().compareTo(o2.getName());
        }
    };
    public static Comparator<People> HeightComparator = new Comparator<>() {
        @Override
        public int compare(People o1, People o2) {
            return o1.getHeight() - o2.getHeight();
        }
    };
    public static Comparator<People> WeightComparator = new Comparator<>() {
        @Override
        public int compare(People o1, People o2) {
            return o1.getWeight() - o2.getWeight();
        }
    };

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        People human = (People) obj;
        if (age != human.age) return false;
        if (height != human.height) return false;
        if (weight != human.weight) return false;
        return name.equals(human.name);
    }

    @Override
    public int hashCode() {
        int a = name == null? 0: name.hashCode();
        a += age + height + weight;
        return a;
    }
}
