package Homeworks.Comparable;

import java.util.Arrays;

public class CompareDemo {
    public static void main(String[] args) {
        People[] newArrayListPeople = new People[3];
        newArrayListPeople[0] = (new People(16, "Asya", 170, 65));
        newArrayListPeople[1] = (new People(30, "Vasiliy", 180, 70));
        newArrayListPeople[2] = (new People(20, "Sa", 200, 90));
        Arrays.sort(newArrayListPeople);
        System.out.println(Arrays.toString(newArrayListPeople));
        Arrays.sort(newArrayListPeople, People.AgeComparator);
        System.out.println("Сортировка по возрасту: ");
        System.out.println(Arrays.toString(newArrayListPeople));
        Arrays.sort(newArrayListPeople, People.NameComparator);
        System.out.println("Сортировка по имени: ");
        System.out.println(Arrays.toString(newArrayListPeople));
        Arrays.sort(newArrayListPeople, People.HeightComparator);
        System.out.println("Сортировка по росту: ");
        System.out.println(Arrays.toString(newArrayListPeople));
        Arrays.sort(newArrayListPeople, People.WeightComparator);
        System.out.println("Сортировка по весу: ");
        System.out.println(Arrays.toString(newArrayListPeople));
    }
}
