package Homeworks;

import java.util.function.BinaryOperator;
import java.util.function.Supplier;

public class FunctionalInterface {
    public static void main(String[] args) {
        System.out.println(randomizer());
    }

    public static Integer calculator(Integer a, Integer b, String o) {
        BinaryOperator<Integer> abc = (x, y) -> {
            switch (o) {
                case "+":
                    return x + y;
                case "-":
                    return x - y;
                case "*":
                    return x * y;
                case "/":
                    return x / y;
                case "^":
                    int result = 1;
                    if (y != 0) {
                        result = x;
                        for (int i = 1; i < y; i++) {
                            result *= x;
                        }
                    }
                    return result;
                default:
                    return 0;
            }
        };
        return abc.apply(a, b);
    }

    public static int randomizer() {
        Supplier<Integer> randomizer = () -> {
            return (int) (Math.random() * 10);
        };
        return randomizer.get();
    }
}