package Homeworks.MultidimensionalArrays_HW;

import java.util.*;

class ArraysHWTask2 {
    private static String StringUtils;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Количество одномерных массивов в двумерном:");
        int z = in.nextInt();
        System.out.println("Длинна массивов:");
        int x = in.nextInt();
        int[][] TDA = new int[z][x];
        for (int i = 0; i < z; i++) {
            for (int j = 0; j < x; j++) {
                System.out.println("Элемент № " + (i + 1) + "." + (j + 1) + ":");
                TDA[i][j] = in.nextInt();
            }
        }
        int o = TDA.length - 2;
        for (int i = 0; i < TDA.length; i++) {
            for (int j = 0; j < TDA.length; j++) {
                if (i == 0 || i == TDA.length - 1) {
                    System.out.print(TDA[i][j] + " ");
                } else if (j == 0 || j == TDA.length - 1) {
                    System.out.print(TDA[i][j] + " ");
                } else {
                    System.out.print(0 + " ");
                }
            }
            System.out.println("");
        }
    }
}