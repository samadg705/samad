package Homeworks.MultidimensionalArrays_HW;

import java.util.Scanner;

class ArraysHWTask3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Количество одномерных массивов в двумерном:");
        int z = in.nextInt();
        System.out.println("Длинна массивов:");
        int x = in.nextInt();
        int[][] TDA = new int[z][x];
        for (int i = 0; i < z; i++) {
            for (int j = 0; j < x; j++) {
                System.out.println("Элемент № " + (i + 1) + "." + (j + 1) + ":");
                TDA[i][j] = in.nextInt();
            }
        }
        int c = 0;
        if (z % 2 == 1) {
            c = TDA.length / 2 + 1;
        } else {
            c = TDA.length / 2;
        }
        for (int i = 0; i < c; i++) {
            for (int j = 0; j < x; j++) {
                int m = TDA[i][j];
                TDA[i][j] = TDA[j][i];
                TDA[j][i] = m;
            }
        }

        for (int i = 0; i < TDA.length; i++) {
            for (int j = 0; j < TDA.length; j++) {
                System.out.print(TDA[i][j] + " ");
            }
            System.out.println("");
        }
    }
}