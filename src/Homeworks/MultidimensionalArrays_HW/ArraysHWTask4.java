package Homeworks.MultidimensionalArrays_HW;

import java.util.*;

class ArraysHWTask4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[][] TDA1 = new int[4][4];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.println("Элемент № 1." + (i + 1) + "." + (j + 1) + ":");
                TDA1[i][j] = in.nextInt();
            }
        }
        int[][] TDA2 = new int[4][4];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.println("Элемент № 2." + (i + 1) + "." + (j + 1) + ":");
                TDA2[i][j] = in.nextInt();
            }
        }
        System.out.println("Сумма матриц: ");
        int[][] TDA3 = new int[4][4];
        for (int i = 0; i < 4; i++) {

            for (int j = 0; j < 4; j++) {

                TDA3[i][j] = TDA1[i][j] + TDA2[i][j];
                System.out.print(TDA3[i][j] + " ");

            }

            System.out.println();

        }
    }
}