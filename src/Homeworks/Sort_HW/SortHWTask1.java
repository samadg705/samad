package Homeworks.Sort_HW;

import java.util.*;

class SortHWTask1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите длину массива: ");
        int a = in.nextInt();
        int[] arr = new int[a];
        for (int i = 0; i < a; i++) {
            System.out.println("Элемент " + (i + 1) + " равен: ");
            arr[i] = in.nextInt();
        }
        for (int i = 0; i < arr.length; i++) {
            int b = arr[i];
            int z = b / 100;
            int x = (b % 100 - b % 10) / 10;
            int c = b % 10;
            if ((z > x) && (x > c)) {
                System.out.println(b);
            }
        }
    }
}