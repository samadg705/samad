package Homeworks.Sort_HW;

import java.util.Scanner;

class SortHWTask2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите длину массива: ");
        int a = in.nextInt();
        int[] arr = new int[a];
        for (int i = 0; i < a; i++) {
            System.out.println("Элемент " + (i + 1) + " равен: ");
            arr[i] = in.nextInt();
        }
        for (int i = 0; i < arr.length; i++) {
            if ((i == 0) || (i == arr.length - 1)) {
                if (i == 0){
                    if (arr[0] > arr[1]) {
                        System.out.println(arr[i] % 2 == 0? arr[i]: "");
                    }
                } else {
                    if (arr[i-1] < arr[i]) {
                        System.out.println(arr[i] % 2 == 0? arr[i]: "");
                    }
                }
            } else {
                if ((arr[i - 1] < arr[i]) && (arr[i] > arr[i + 1])) {
                    System.out.println(arr[i] % 2 == 0? arr[i]: "");
                }
            }
        }
    }
}