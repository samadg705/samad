package Homeworks.Sort_HW;

import java.util.Arrays;
import java.util.Scanner;

class SortHWTask5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите длину массива: ");
        int a = in.nextInt();
        int[] arr = new int[a];
        for (int i = 0; i < a; i++) {
            System.out.println("Элемент " + (i + 1) + " равен: ");
            arr[i] = in.nextInt();
        }
        for (int i = 0; i < arr.length; i++) {
            int q = arr[i];
            int j = i - 1;
            for (; j >= 0; j--) {
                if (q < arr[j]) {
                    arr[j + 1] = arr[j];
                } else {
                    break;
                }
            }
            arr[j + 1] = q;
        }
        System.out.println(Arrays.toString(arr));
    }
}