package Homeworks.Sort_HW;

import java.util.Scanner;

class SortHWTask4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите число: ");
        int b = in.nextInt();
        System.out.println("Введите длину массива: ");
        int a = in.nextInt();
        int[] arr = new int[a];
        for (int i = 0; i < a; i++) {
            System.out.println("Элемент " + (i + 1) + " равен: ");
            arr[i] = in.nextInt();
        }
    }
}