package Homeworks.PolymorphismnAndAbstractClasses;

import static java.lang.Math.sin;

public class Rhombus extends Parallelogram {
    private final int a;
    private int c;
    private int d;

    public Rhombus(int a, int c, int d) {
        this.a = a;
        this.c = c;
        this.d = d;
    }

    public double getSquare() {
        return a * a * sin(c);
    }
}
