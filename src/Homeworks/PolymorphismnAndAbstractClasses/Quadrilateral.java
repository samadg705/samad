package Homeworks.PolymorphismnAndAbstractClasses;

public abstract class Quadrilateral {
    public abstract double getSquare();
}
