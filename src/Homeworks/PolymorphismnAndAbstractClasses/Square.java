package Homeworks.PolymorphismnAndAbstractClasses;

public class Square extends Quadrilateral {
    private final int a;

    public Square(int a) {
        this.a = a;
    }

    @Override
    public double getSquare() {
        return a * a;
    }
}
