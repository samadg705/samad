package Homeworks.PolymorphismnAndAbstractClasses;

public class Rectangle extends Quadrilateral {
    private final int a;
    private final int b;

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double getSquare() {
        return a * b;
    }
}
