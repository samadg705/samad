package Homeworks.PolymorphismnAndAbstractClasses;

public class AbstractHWMain {
    private static double[] Squares = new double[4];

    public static void main(String[] args) {
        Square Square = new Square(4);
        Parallelogram Parallelogram = new Parallelogram(2, 4, 80, 100);
        Rectangle Rectangle = new Rectangle(2, 4);
        Rhombus Rhombus = new Rhombus(4, 50, 130);
        Quadrilateral[] figures = new Quadrilateral[4];
        figures[0] = Square;
        figures[1] = Parallelogram;
        figures[2] = Rectangle;
        figures[3] = Rhombus;
        for (int i = 0; i < figures.length; i++) {
            Squares[i] = figures[i].getSquare();
        }
        System.out.println(getSquares());
    }

    private static double[] getSquares() {
        return Squares;
    }

}
