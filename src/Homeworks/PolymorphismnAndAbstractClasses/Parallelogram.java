package Homeworks.PolymorphismnAndAbstractClasses;

import static java.lang.Math.sin;
public class Parallelogram extends Quadrilateral {
    private int a;
    private int b;
    private int c;
    private int d;

    public Parallelogram(int a, int b, int c, int d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public Parallelogram() {
    }

    @Override
    public double getSquare() {
        return a * b * sin(c);
    }
}
