package Homeworks.Static;

import java.util.Scanner;

public class MyArrayUtils {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Сортировка массива -- 1");
        System.out.println("Поиск подстроки в массиве строк -- 2");
        System.out.println("Факториал числа -- 3");
        int x = in.nextInt();
        switch (x) {
            case 1:
                int[] IntArray = new int[5];
                System.out.println("Заполните массив");
                for (int i = 0; i < IntArray.length; i++) {
                    System.out.println((i + 1) + "/5 :");
                    IntArray[i] = in.nextInt();
                }
                IntArray = sort(IntArray);
                for (int j : IntArray) {
                    System.out.print(j + " ");
                }
                break;
            case 2:
                String[] StringArray = new String[3];
                StringArray[0] = "java";
                StringArray[1] = "python";
                StringArray[2] = "c++";
                System.out.println("Введите подстроку");
                String SubString = in.next();
                System.out.println(bool(StringArray, SubString));
                break;
            case 3:
                System.out.println("Введите число");
                int number = in.nextInt();
                System.out.println(factorial(number));
                break;
            default:
                System.out.println("Такого метода нет :(");
                break;
        }
    }

    public static int[] sort(int[] array) {
        int NeZnauKakNazvat;
        for (int InfiniteLoop = 0; InfiniteLoop <= 9999999; InfiniteLoop++) {
            for (int i = 1; i < array.length; i++) {
                if (array[i] < array[i - 1]) {
                    NeZnauKakNazvat = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = NeZnauKakNazvat;
                }
            }
            int x = 0;
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] < array[j + 1]) {
                    x++;
                }
            }
            if (x == array.length - 1) {
                break;
            }
        }
        return array;
    }

    public static boolean bool(String[] array, String SubString) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].contains(SubString)) {
                return true;
            }
        }
        return false;
    }

    public static int factorial(int number) {
        int number2 = number - 1;
        for (int i = number2; i > 1; i--) {
            number = number * i;
        }
        return number;
    }
}
