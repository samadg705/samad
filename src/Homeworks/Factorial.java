package Homeworks;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int count = in.nextInt();
        System.out.println(f(count));
    }

    static int f(int n1) {
        if (n1 == 1 || n1 == 0) {
            return 1;
        }
        return n1 * f(n1 -1);
    }
}
