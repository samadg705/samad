package Homeworks.Enum;

import java.util.ArrayList;
import java.util.Scanner;

public class EnumHWMain {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("метод 1");
        Phone phone1 = new Phone(Colors.BLACK, "+79992687141", 200);
        method(phone1);
        System.out.println("метод 2");
        ArrayList<Phone> phones = new ArrayList<>();
        Phone phone2 = new Phone(Colors.WHITE, "+79992687141", 200);
        Phone phone3 = new Phone(Colors.PINK, "+79673737348", 250);
        Phone phone4 = new Phone(Colors.BLUE, "+79999999999", 150);
        phones.add(phone2);
        phones.add(phone3);
        phones.add(phone4);
        String string = in.nextLine();
        ArrayList<Phone> phones1 = method2(phones, string);
        for (Phone phone: phones1) {
            System.out.println(phone);
        }
        System.out.println("метод 3");
        ArrayList<Phone> phones2 = method3(phones);
        for (Phone phone: phones2) {
            System.out.println(phone);
        }
    }
    public static void method(Phone phone) {
        switch (phone.getColor()) {
            case BLACK -> System.out.println("black");
            case WHITE -> System.out.println("white");
            case BLUE -> System.out.println("blue");
            case PINK -> System.out.println("pink");
        }
    }
    public static ArrayList<Phone> method2(ArrayList<Phone> phones, String string) {
        ArrayList<Phone> phones2 = new ArrayList<>();
        for(Phone phone: phones) {
            if (phone.getColor().toString().equals(string)) {
                phones2.add(phone);
            }
        }
        return phones2;
    }
    public static ArrayList<Phone> method3(ArrayList<Phone> phones) {
        ArrayList<Phone> phones2 = new ArrayList<>();
        for(Phone phone: phones) {
            if ((phone.getColor().toString()).contains("I")) {
                phones2.add(phone);
            }
        }
        return phones2;
    }
}
