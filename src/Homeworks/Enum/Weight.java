package Homeworks.Enum;

public enum Weight {
    ONEHUNDRED,
    ONEHUNDREDANDFIFTY,
    TWOHUNDRED,
    TWOHUNDREDANDFIFTY,
    THREEHUNDRED
}
