package Homeworks.Enum;

public class Phone {
    private Colors color;
    private String number;
    private int weight;


    public Phone(Colors color, String number, int weight) {
        this.color = color;
        this.number = number;
        this.weight = weight;
    }

    public void setColor(Colors color) {
        this.color = color;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Colors getColor() {
        return color;
    }

    public String getNumber() {
        return number;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "color=" + color +
                ", number='" + number + '\'' +
                ", weight=" + weight +
                '}';
    }
}
