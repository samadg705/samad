package Homeworks.MA_HW2;

import java.util.Scanner;

class ArraysHW2Task1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the month number");
        int a = in.nextInt();
        String month;
        switch (a) {
            case 1:
                month = "January";
                System.out.println(month);
                break;
            case 2:
                month = "February";
                System.out.println(month);
                break;
            case 3:
                month = "March";
                System.out.println(month);
                break;
            case 4:
                month = "April";
                System.out.println(month);
                break;
            case 5:
                month = "May";
                System.out.println(month);
                break;
            case 6:
                month = "June";
                System.out.println(month);
                break;
            case 7:
                month = "July";
                System.out.println(month);
                break;
            case 8:
                month = "August";
                System.out.println(month);
                break;
            case 9:
                month = "September";
                System.out.println(month);
                break;
            case 10:
                month = "October";
                System.out.println(month);
                break;
            case 11:
                month = "November";
                System.out.println(month);
                break;
            case 12:
                month = "December";
                System.out.println(month);
                break;
            default:
                month = "You made a mistake:(";
                System.out.println(month);
                break;
        }
    }
}