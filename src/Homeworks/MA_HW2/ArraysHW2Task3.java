package Homeworks.MA_HW2;

import java.util.*;

class ArraysHW2Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int x = in.nextInt();
        int count = 0;

        int [][] table = new int[n][n];

        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                table[i][j] = (i + 1) * (j + 1);
            }
        }

        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                if (table[i][j] == x) {
                    count ++;
                }
            }
        }
        System.out.println(count);
    }
}