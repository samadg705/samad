package Threads;

public class Main1 {
    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread();
        Thread thread2 = new Thread();
        Thread thread3 = new Thread();
        while (true) {
            System.out.println(thread1.getName());
            Thread.sleep((long) ((Math.random() * 900) + 100));
            thread1.join();
            System.out.println(thread2.getName());
            Thread.sleep((long) ((Math.random() * 900) + 100));
            thread2.join();
            System.out.println(thread3.getName());
            Thread.sleep((long) ((Math.random() * 900) + 100));
            thread3.join();
        }
    }
}
