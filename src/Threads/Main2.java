package Threads;

public class Main2 {
    public static class Thread2 extends Thread {
        public void run(Thread thread2) throws InterruptedException {
            thread2.start();
            Thread2.sleep(5000);
            if (thread2.isAlive()) {
                thread2.interrupt();
                System.out.println("Не успел");
            } else {
                System.out.println("Успел");
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Runnable runnable2 = () -> {
            try {
                Thread.sleep((long) (((Math.random() * 1000) + 500) + ((Math.random() * 1000) + 500) +
                        ((Math.random() * 1000) + 500) + ((Math.random() * 1000) + 500) + ((Math.random() * 1000) + 500)));
            } catch (InterruptedException e) {}
        };
        Thread thread2 = new Thread(runnable2);
        Runnable runnable1 = () -> {
            thread2.start();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            if (thread2.isAlive()) {
                thread2.interrupt();
                System.out.println("Не успел");
            } else {
                System.out.println("Успел");
            }
        };

        Thread thread1 = new Thread(runnable1);
        thread1.start();
    }
}
