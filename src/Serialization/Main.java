package Serialization;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("C:\\Samad\\IdeaProjects\\NewProject\\src\\Serialization\\resourses\\save.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            Class2 a = new Class2(1, 2);
            Class3 b = new Class3(5, 6);
            objectOutputStream.writeObject(a);
            objectOutputStream.writeObject(b);
            objectOutputStream.close();
            FileInputStream fileInputStream = new FileInputStream("C:\\Samad\\IdeaProjects\\NewProject\\src\\Serialization\\resourses\\save.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            Class2 x = (Class2) objectInputStream.readObject();
            Class3 y = (Class3) objectInputStream.readObject();
            System.out.println(x + ", " + y);
            objectInputStream.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
