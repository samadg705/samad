package Serialization;

public class Class1 {
    int a;
    int b;

    public Class1(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public Class1() {
    }

    @Override
    public String toString() {
        return a + ", " + b;
    }
}
