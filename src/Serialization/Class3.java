package Serialization;

import java.io.Serializable;

public class Class3 extends Class1 implements Serializable {
    public Class3(int a, int b) {
        super(a, b);
    }

    @Override
    public String toString() {
        return a + ", " + b;
    }
}
