package SomeTasks;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Tasks {
    public static void main(String[] args) {
        System.out.println(calculator(reverseCalculator("2 3 6 1 / * -")));
    }

    public static boolean checkBrackets(String string) {
        if (string.length() <= 1) return false;
        Stack<Character> newStack = new Stack<>();
        char[] string2 = string.toCharArray();
        for (Character char2 : string2) {
            if (char2.equals('(') || char2.equals('{') || char2.equals('[')) {
                newStack.add(char2);
            } else if (char2.equals(')') || char2.equals('}') || char2.equals(']')) {
                if (newStack.size() == 0) return false;
                switch (char2) {
                    case ')':
                        if (newStack.pop() != '(') return false;
                        break;
                    case '}':
                        if (newStack.pop() != '{') return false;
                        break;
                    case ']':
                        if (newStack.pop() != '[') return false;
                        break;
                }
            }
        }
        return true;
    }

    public static String reverseCalculator(String s) throws IllegalArgumentException {
        String result = "";
        Queue<Character> queue = new LinkedList<>();
        String s2 = "";
        for (int i = s.length() - 1; i >= 0; i -= 2) {
            if (s.charAt(i) == '+' || s.charAt(i) == '-' || s.charAt(i) == '*' || s.charAt(i) == '/') {
                queue.add(s.charAt(i));
            } else {
                s2 = s.substring(0, i + 1);
                break;
            }
        }
        for (int i = 0; i < s2.length(); i++) {
            if (s2.charAt(i) != ' ') {
                result += s2.charAt(i);
            } else {
                result += " " + queue.poll() + " ";
            }
        }
        result += " ";
        return result;
    }

    public static Integer calculator(String s) {
        ArrayList<Integer> a = new ArrayList<>();
        Pattern pattern1 = Pattern.compile("\\d+?\s");
        Matcher matcher1 = pattern1.matcher(s);
        while (matcher1.find()) {
            a.add(Integer.parseInt((s.substring(matcher1.start(), matcher1.end())).trim()));
        }
        ArrayList<String> b = new ArrayList<>();
        Pattern pattern2 = Pattern.compile("[-/+*]");
        Matcher matcher2 = pattern2.matcher(s);
        while (matcher2.find()) {
            b.add(s.substring(matcher2.start(), matcher2.end()));
        }
        for (int i = 0; i < b.size();) {
            if (b.get(i).equals("*") || b.get(i).equals("/")) {
                if (b.get(i).equals("*")) {
                    a.set(i, a.get(i) * a.get(i + 1));
                    a.remove(i + 1);
                    b.remove(i);
                } else if (b.get(i).equals("/")) {
                    a.set(i, a.get(i) / a.get(i + 1));
                    a.remove(i + 1);
                    b.remove(i);
                }
            } else {
                i++;
            }
        }
        for (int i = 0; i < b.size(); i++) {
            if (b.get(i).equals("+") || b.get(i).equals("-")) {
                if (b.get(i).equals("+")) {
                    a.set(i, a.get(i) + a.get(i + 1));
                    a.remove(i + 1);
                    b.remove(i);
                } else if (b.get(i).equals("-")) {
                    a.set(i, a.get(i) - a.get(i + 1));
                    a.remove(i + 1);
                    b.remove(i);
                } else {
                    i++;
                }
            }
        }
        return a.get(0);
    }
}
