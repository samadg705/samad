package Quiz;

public class QuizOneTwo {

    public static void main(String[] args) {
        System.out.println(toKString("aabaabaabaab", 4));
    }

    //Task 1
    public static int convertToDec(int number, int k) {
        int result = 0;
        int step = 0;
        while (number > 0) {
            if (number % 10 < k) {
                result += (number % 10) * pow(k, step);
                number /= 10;
                step++;
            } else {
                return 0;
            }
        }
        return result;
    }

    //Task 2
    public static double countCombinations(int n, int m) {
        return factorial((n + m -1)) / (factorial(n - 1) * factorial(m));
    }
    //Task 3
    public static String toKString(String s, int k) {
        if (k == 1) {
            return s;
        } else if (k < s.length() && s.length() % k == 0) {
            char x;
            for (int i = 0; i < s.length() / k; i++) {
                x = s.charAt(i);
                for (int j = i; j < s.length(); j += s.length() / k) {
                    if (x != s.charAt(j)) {
                        return null;
                    }
                }
            }
            String newS = "";
            for (int i = 0; i < s.length() / k; i++) {
                newS += s.charAt(i);
            }
            return newS;
        } else if (k == s.length()){
            char j = s.charAt(0);
            for (int i = 1; i < s.length(); i++) {
                if (j != s.charAt(i)) {
                    return null;
                }
            }
        }
        return null;
    }
    public static int pow(int k, int step) {
        int result = 1;
        for (int i = 0; i < step; i++) {
            result *= k;
        }
        return result;
    }
    public static int factorial(int x) {
        if (x == 1 || x == 0) {
            return 1;
        }
        return x * factorial(x -1);
    }
}
