package Exception;

import java.util.Locale;

public class MainException {
    public static void main(String[] args) {
        System.out.println(method(null));
    }
    public static String method(String string) {
        try {
            string = string.toUpperCase(Locale.ROOT);
        } catch(NullPointerException e) {
            throw new NullPointerException("строки нет");
        } finally {
            System.out.println("...");
        }
        return string;
    }
}
