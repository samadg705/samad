package Player;

import java.util.ArrayList;
import java.util.Collections;

public class Player {
    private ArrayList<String> title = new ArrayList<>();
    public void sort() {
        Collections.sort(this.title);
        print();
    }
    public void add(String music) {
        title.add(music);
    }
    public void print() {
        for (String music: title) {
            System.out.println(music);
        }
    }

    public Player(ArrayList<String> title) {
        this.title = title;
    }
}
