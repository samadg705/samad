package InputOutputFile;

import java.io.*;
import java.nio.file.NoSuchFileException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        String[] strings = {"a", "b", "c", "d", "Hello World!", "Goodbye World!"};
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("C:\\Samad\\IdeaProjects\\NewProject\\src\\InputOutputFile\\text.txt"))) {
            for (String str: strings) {
                bufferedWriter.write(str);
            }
        } catch (IOException exception) {
            throw new FileNotFoundException("Файл не найден");
        }

    }

}
