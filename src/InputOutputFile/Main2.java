package InputOutputFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;

public class Main2 {
    public static void main(String[] args) throws IOException {
        /*
        People people1 = new People(13, "Samad", 154, 36);
        People[] peoples = new People[3];
        peoples[0] = people1;
        peoples[1] = new People(13, "Murad", 165, 45);
        peoples[2] = new People(15, "Petya", 170, 55);
        People[] peoples2 = new People[3];
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("C:\\Samad\\IdeaProjects\\NewProject\\src\\InputOutputFile\\text2.txt"))) {
            objectOutputStream.writeObject(peoples[0]);
            objectOutputStream.writeObject(peoples[1]);
            objectOutputStream.writeObject(peoples[2]);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("C:\\Samad\\IdeaProjects\\NewProject\\src\\InputOutputFile\\text2.txt"))) {
            peoples2[0] = (People) objectInputStream.readObject();
            peoples2[1] = (People) objectInputStream.readObject();
            peoples2[2] = (People) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        for (People people : peoples2) {
            System.out.println(people.toString());
        }
        */
        method2("C:\\Samad\\IdeaProjects\\NewProject\\src\\InputOutputFile\\csv\\dataexport2.csv");
    }

    public static void method(String path) throws IOException {
        Path of = Path.of(path);
        try {
            File file = new File(path);
            if (file.createNewFile()) {
                System.out.println("Файл успешно создан");
            } else {
                Files.createDirectory(of);
                System.out.println("Папка успешно создана");
            }
        } catch (IOException e) {
            Files.createFile(of);
        }
    }

    public static void method2(String path) {
        BufferedReader br;
        String line;
        int count = 0;
        double temperature = 0;
        double relativeHumidity = 0;
        double windSpeed = 0;
        String dateMaxTemperature = null;
        double maxTemperature = 0;
        String dateMinRelativeHumidity = null;
        double minRelativeHumidity = 0;
        String dateMaxWindSpeed = null;
        double maxWindSpeed = 0;
        int N = 0;
        int S = 0;
        int W = 0;
        int E = 0;
        int NW = 0;
        int NE = 0;
        int SW = 0;
        int SE = 0;

        try {
            br = new BufferedReader(new FileReader(path));
            while ((line = br.readLine()) != null) {
                String[] str = line.split(",");
                String str0 = str[0];
                double int1 = Double.parseDouble(str[1]);
                double int2 = Double.parseDouble(str[2]);
                double int3 = Double.parseDouble(str[3]);
                double int4 = Double.parseDouble(str[4]);
                count++;
                temperature += int1;
                relativeHumidity += int2;
                windSpeed += int3;
                if (int1 > maxTemperature) {
                    maxTemperature = int1;
                    dateMaxTemperature = str0;
                }
                if ((minRelativeHumidity > int2) || (minRelativeHumidity == 0)) {
                    minRelativeHumidity = int2;
                    dateMinRelativeHumidity = str0;
                }
                if ((maxWindSpeed < int3) || (maxWindSpeed == 0)) {
                    maxWindSpeed = int3;
                    dateMaxWindSpeed = str0;
                }
                if ((int4 >= 0 && int4 <= 22) || (int4 >= 337 && int4 <= 360)) {
                    N++;
                } else if (int4 > 22 && int4 < 67) {
                    NE++;
                } else if (int4 >= 67 && int4 <= 112) {
                    E++;
                } else if (int4 > 112 && int4 < 157) {
                    SE++;
                } else if (int4 >= 157 && int4 <= 202) {
                    S++;
                } else if (int4 > 202 && int4 < 247) {
                    SW++;
                } else if (int4 >= 247 && int4 <= 292) {
                    W++;
                } else if (int4 > 292 && int4 < 337) {
                    NW++;
                }
            }
            temperature = temperature / count;
            relativeHumidity = relativeHumidity / count;
            windSpeed = windSpeed / count;
            BufferedWriter bw = new BufferedWriter(new FileWriter("C:\\Samad\\IdeaProjects\\NewProject\\src\\InputOutputFile\\csv\\result"));
            bw.write(temperature + ", " + relativeHumidity + ", " + windSpeed);
            bw.newLine();
            bw.write(dateMaxTemperature.substring(6, 11) + ":00, " + maxTemperature);
            bw.newLine();
            bw.write(dateMinRelativeHumidity.substring(6, 11) + ":00, " + minRelativeHumidity);
            bw.newLine();
            bw.write(dateMaxWindSpeed.substring(6, 11) + ":00, " + maxWindSpeed);
            bw.newLine();
            ArrayList<Integer> list = new ArrayList<>();
            list.add(N);
            list.add(NE);
            list.add(E);
            list.add(SE);
            list.add(S);
            list.add(SW);
            list.add(W);
            list.add(NW);
            if (N == Collections.max(list)) bw.write("N");
            if (NE == Collections.max(list)) bw.write("NE");
            if (E == Collections.max(list)) bw.write("E");
            if (SE == Collections.max(list)) bw.write("SE");
            if (S == Collections.max(list)) bw.write("S");
            if (SW == Collections.max(list)) bw.write("SW");
            if (W == Collections.max(list)) bw.write("W");
            if (NW == Collections.max(list)) bw.write("NW");
            br.close();
            bw.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
