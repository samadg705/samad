package Collections.Company;

import java.util.*;

public class Company {
    private final int INIT_SIZE = 5;
    private List<Employee> employees;
    private double income = 0;

    public Company(List<Employee> employees) {
        this.employees = employees;
        for (Employee employee: employees) {
            employee.setCompany(this);
        }
        employeesSetCompany();
        income();
    }

    void hire(Employee employee) {
        employees.add(employee);
        employee.setCompany(this);
    }

    void hireAll(List<Employee> employees2) {
        for (Employee employee : employees2) {
            employees.add(employee);
        }
    }

    void fair(Employee employee) {
        employees.remove(employee);
    }

    Employee[] getTopSalaryStaff(int count) {
        Employee[] employees1 = new Employee[count];
        Comparator<Employee> compareBySalary = (o1, o2) -> (int) (o1.getMonthSalary() - o2.getMonthSalary());
        employees.sort(compareBySalary);
        for (int i = 0; i < count; i++) {
            employees1[i] = employees.get(i);
        }
        return employees1;
    }

    Employee[] getLowestSalaryStaff(int count) {
        Employee[] employees1 = new Employee[count];
        Comparator<Employee> compareBySalary = (o1, o2) -> (int) (o2.getMonthSalary() - o1.getMonthSalary());
        employees.sort(compareBySalary);
        for (int i = 0; i < count; i++) {
            employees1[i] = employees.get(i);
        }
        return employees1;
    }

    double getIncome() {
        return income;
    }

    void income() {
        for (Employee employee: employees) {
            if (employee instanceof Manager) {
                this.income += ((Manager) employee).income;
            }
        }
    }
    void employeesSetCompany() {
        for (Employee employee: employees) {
            employee.setCompany(this);
        }
    }
}
