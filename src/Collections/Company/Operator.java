package Collections.Company;

public class Operator implements Employee {
    Company company;
    String name;
    double salary;

    public Operator(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    @Override
    public double getMonthSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return name + ", Operator, " + salary;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
