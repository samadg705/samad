package Collections.Company;

public class Manager implements Employee {
    Company company;
    String name;
    double income;
    double salary;

    public Manager(String name, double salary) {
        this.name = name;
        this.income = Math.random() * 25000 + 115000;
        this.salary = salary + income * 0.05;
    }

    @Override
    public double getMonthSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return name + ", Manager, " + salary;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
