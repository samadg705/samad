package Collections.Company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int a = 180;
        Random random = new Random();
        List<Employee> employees = new ArrayList<>();
        String[] names = new String[5];
        names[0] = "Sanya";
        names[1] = "Petya";
        names[2] = "Vanya";
        names[3] = "Murad";
        names[4] = "Alan";
        while (a >= 0) {
            employees.add(new Operator(names[random.nextInt(5)], 50000));
            a--;
        }
        a = 80;
        while (a >= 0) {
            employees.add(new Manager(names[random.nextInt(5)], 80000));
            a--;
        }
        a = 10;
        while (a >= 0) {
            employees.add(new TopManager(names[random.nextInt(5)], 100000));
            a--;
        }
        Company company = new Company(employees);
        for (Employee employee: company.getTopSalaryStaff(15)) {
            System.out.println(employee.getMonthSalary());
        }
        for (Employee employee: company.getLowestSalaryStaff(30)) {
            System.out.println(employee.getMonthSalary());
        }
    }
}
