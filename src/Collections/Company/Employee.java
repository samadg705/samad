package Collections.Company;

public interface Employee {
    double getMonthSalary();
    void setCompany(Company company);
}
