package Collections.Company;

public class TopManager implements Employee {
    Company company;
    String name;
    double salary;

    public TopManager(String name, double salary) {
        this.name = name;
        if (company.getIncome() >= 10000000) {
            this.salary = salary + salary * 1.5;
        } else {
            this.salary = salary;
        }
    }

    @Override
    public double getMonthSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return name + ", TopManager, " + salary;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
