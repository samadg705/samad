package Collections;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HashMapClass {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        HashMap<Character, Integer> aHashMap = new HashMap<>();
        String aString = in.nextLine();
        char[] aCharArray = aString.toCharArray();
        for (int i = 0; i < aString.length(); i++) {
            if (!aHashMap.containsKey(aCharArray[i])) {
                aHashMap.put(aCharArray[i], 1);
            } else {
                aHashMap.put(aCharArray[i], aHashMap.get(aCharArray[i]) + 1);
            }
        }
        for (Map.Entry<Character, Integer> entry : aHashMap.entrySet()) {
            System.out.println("Key: " + entry.getKey() + " Value: " + entry.getValue());
        }
    }
}
