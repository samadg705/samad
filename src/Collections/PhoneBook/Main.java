package Collections.PhoneBook;

import java.util.Scanner;

public class Main {
    static PhoneBook phoneBook = new PhoneBook();

    static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            System.out.println("Введите номер, имя или команду");
            String input = in.nextLine();
            if (input.equals("QUITE")) {
                break;
            } else if (input.equals("LIST")) {
                phoneBook.list();
            } else if (PhoneBook.checkCorrectName(input)) {
                phoneBook.addByName(input);
            } else if (PhoneBook.checkCorrectNumber(input)) {
                phoneBook.addByNumber(input);
            } else {
                System.out.println("Неверный формат ввода");
            }
        }
    }
}
