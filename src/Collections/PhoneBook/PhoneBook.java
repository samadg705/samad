package Collections.PhoneBook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneBook {
    static Scanner in = new Scanner(System.in);
    static HashMap<String, String> book = new HashMap<>();

    public static void addByNumber(String number) {
        if (book.containsKey(number)) {
            System.out.println(book.get(number));
        } else {
            System.out.println("Такого номера нет в телефонной книге");
            System.out.println("Введите имя абонента для номера \"" + number + "\"");
            String name = in.nextLine();
            if (checkCorrectNumber(number) && checkCorrectName(name)) {
                book.put(number, name);
                System.out.println("Контакт сохранен");
            } else {
                System.out.println("Некорректный номер");
            }
        }
    }
    public static void addByName(String name) {
        if (book.containsValue(name)) {
            for (String number: book.keySet()) {
                if (book.get(number).equals(name)) {
                    System.out.println(number);
                }
            }
        } else {
            System.out.println("Такого имени нет в телефонной книге");
            System.out.println("Введите номер телефона для абонента \"" + name + "\"");
            String number = in.nextLine();
            if (checkCorrectNumber(number) && checkCorrectName(name)) {
                book.put(number, name);
                System.out.println("Контакт сохранен");
            } else {
                System.out.println("Некорректное имя");
            }
        }
    }
    public static void list() {
        ArrayList<String> names = new ArrayList<>();
        for (String name : book.values()){
            if (!names.contains(name)) {
                System.out.print(name + " - ");
                for (String number : book.keySet()) {
                    if (book.get(number).equals(name)) {
                        System.out.print(number + ", ");
                    }
                }
                System.out.println();
            }
            names.add(name);
        }
    }
    public static boolean checkCorrectNumber(String number) {
        Pattern pattern = Pattern.compile("\\d{11,}");
        Matcher matcher = pattern.matcher(number);
        String number2 = "";
        while (matcher.find()) {
            number2 += number.substring(matcher.start(), matcher.end());
        }
        return number.length() == number2.length();
    }
    public static boolean checkCorrectName(String name) {
        Pattern pattern = Pattern.compile("[a-zA-Zа-яА-Я]+");
        Matcher matcher = pattern.matcher(name);
        String name2 = "";
        while (matcher.find()) {
            name2 += name.substring(matcher.start(), matcher.end());
        }
        return name.equals(name2);
    }

}
